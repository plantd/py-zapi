import unittest


class TestClient(unittest.TestCase):
    def setUp(self):
        pass

    def test_foo(self):
        self.assertEqual("foo".upper(), "FOO")


if __name__ == "__main__":
    unittest.main()
